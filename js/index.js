$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 1000
    });

    $('#contacto').on('show.bs.modal', function(e){
      console.log('El modal se empezó a abrirse');

      $('.contactoBtn').removeClass('btn-outline-success');
      $('.contactoBtn').addClass('btn-dark');
      $('.contactoBtn').prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function(e){
      console.log('El modal se abrió');
    });

    $('#contacto').on('hide.bs.modal', function(e){
      console.log('El modal empezó a cerrarse');
    });

    $('#contacto').on('hidden.bs.modal', function(e){
      console.log('El modal se cerró');
      $('.contactoBtn').removeClass('btn-dark');
      $('.contactoBtn').addClass('btn-outline-success');
      $('.contactoBtn').prop('disabled', false);
    });

  });